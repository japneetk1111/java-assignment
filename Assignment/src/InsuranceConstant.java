public class InsuranceConstant {
    final static int HATCHBACK_PERCENT = 5;
    final static int SEDAN_PERCENT = 8;
    final static int  SUV_PERCENT = 10;
    final static int  PREMIUM_PERCENT = 20;
}