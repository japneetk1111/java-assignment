
public class CalculateInsuranceCost {
	public int cost = 0;
	
	private int calculateCost(String carType, int carCost, String carInsuranceType) {
		
		int percent = -1;
		
		if(carType.equalsIgnoreCase("hatchback")) 
			percent = InsuranceConstant.HATCHBACK_PERCENT;
		else if(carType.equalsIgnoreCase("sedan")) 
			percent = InsuranceConstant.SEDAN_PERCENT;
		else if(carType.equalsIgnoreCase("suv")) 
			percent = InsuranceConstant.SUV_PERCENT;
		
		if(percent == -1) {
			System.out.println("Invalid Car Type");
			return 0;
		}
		
		cost += (int)(carCost/100) * percent;
		
		boolean validInsurance = false;
		
		if(carInsuranceType.equalsIgnoreCase("premium")) {
			cost += (int)(cost/100) * InsuranceConstant.PREMIUM_PERCENT;
			validInsurance = true;
		} else if(carInsuranceType.equalsIgnoreCase("basic")) {
			validInsurance = true;
		} 
		
		if(!validInsurance){
			System.out.println("Invalid Car Insurance Type");
			return 0;
		}
		
		return cost;
	}
	
	public int calculateInsurancePrice(String carType, int carCost, String carInsuranceType) {
		return calculateCost(carType, carCost, carInsuranceType);
	}
}
