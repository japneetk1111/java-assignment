import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarInsurance {
	
	public static Scanner scn = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char isYes = 'Y';
		while(isYes == 'Y' || isYes == 'y' ) {
			System.out.println("\nEnter Details of a Car ");
			System.out.print("Enter Car Model : ");
			String carModel  = scn.next();
			System.out.print("Enter Car Type : ");
			String carType = scn.next();
			System.out.print("Enter Car Cost : ");
			int carCost = scn.nextInt();
			System.out.print("Enter Car Insurance Type : ");
			String carInsuranceType = scn.next();
			Car car = new Car(carModel, carType, carCost, carInsuranceType);
			int effectiveInsurancePremium = car.getCarInsurancePremiumCost();
			if(effectiveInsurancePremium != 0 || effectiveInsurancePremium != -1)
				System.out.println("Your Effective Insurance Premium : Rs" + effectiveInsurancePremium);
			System.out.println("\nDo you want to enter details of any other car (y/n): ");
			isYes = scn.next().charAt(0);
		}
	}

}
