
public class Car {
	String carModel;
	String carType;
	int carCost;
	String carInsuranceType;
	
	Car(String carModel, String carType, int carCost, String carInsuranceType) {
		this.carModel = carModel;
		this.carType = carType;
		this.carCost = carCost;
		this.carInsuranceType = carInsuranceType;
	}
	
	int getCarInsurancePremiumCost() {
		CalculateInsuranceCost cost = new CalculateInsuranceCost();
		return cost.calculateInsurancePrice(carType, carCost, carInsuranceType);
	}
}
